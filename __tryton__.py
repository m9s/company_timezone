# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
{
    'name': 'Company Timezone',
    'name_de_DE': 'Unternehmen Zeitzone',
    'version': '2.2.0',
    'author': 'MBSolutions',
    'email': 'info@m9s.biz',
    'website': 'http://www.m9s.biz',
    'description': '''
    - Adds field timezone to company.
    - Adds some current routines for convertion of user or company time to
      local time (server time).
    ''',
    'description_de_DE': '''
    - Fügt Feld Zeitzone für das Unternehmen hinzu.
    - Fügt einige Routinen für die Konvertierung der Benutzer- oder
      Unternehmenszeit in lokale Zeit (Serverzeit) hinzu.
    ''',
    'depends': [
        'company',
    ],
    'xml': [
        'company.xml',
    ],
    'translation': [
        'locale/de_DE.po',
    ],
}
