# This file is part of Tryton.  The COPYRIGHT file at the top level
# of this repository contains the full copyright notices and license terms.
import pytz
import datetime
import dateutil.tz
from trytond.model import Model
from trytond.pool import Pool
from trytond.transaction import Transaction

class Date(Model):
    _name = 'ir.date'

    def local2companytime(self, company, date, tzinfo=False):
        '''
        Return a datetime converted from server to company timezone.

        :param company: a company id
        :param date: a date or datetime
        :param tzinfo: a boolean to indicate, whether datetime should be
                        returned with timezone
        :return: a datetime
        '''
        company_obj = Pool().get('company.company')
        tzlocal = dateutil.tz.tzlocal()

        company = company_obj.browse(company)
        tzcomp = tzlocal
        if company.timezone:
            tzcomp = dateutil.tz.gettz(company.timezone)
        if not isinstance(date, datetime.datetime):
            date = datetime.datetime.combine(date, datetime.time())\
                    .replace(tzinfo=tzlocal)
        else:
            date = date.replace(tzinfo=tzlocal)
        date = date.astimezone(tzcomp)
        if not tzinfo:
            date = date.replace(tzinfo=None)
        return date

    def company2localtime(self, company, date, tzinfo=False):
        '''
        Return a datetime converted from company to server timezone.

        :param company: a company id
        :param date: a date or datetime
        :param tzinfo: a boolean to indicate, whether datetime should be
                        returned with timezone
        :return: a datetime
        '''
        company_obj = Pool().get('company.company')
        tzlocal = dateutil.tz.tzlocal()

        company = company_obj.browse(company)
        tzcomp = tzlocal
        if company.timezone:
            tzcomp = dateutil.tz.gettz(company.timezone)
        if not isinstance(date, datetime.datetime):
            date = datetime.datetime.combine(date, datetime.time())\
                    .replace(tzinfo=tzcomp)
        else:
            date = date.replace(tzinfo=tzcomp)
        date = date.astimezone(tzlocal)
        if not tzinfo:
            date = date.replace(tzinfo=None)
        return date

    def local2usertime(self, user, date, tzinfo=False):
        '''
        Return a datetime converted from server to user timezone.

        :param user: a user id
        :param date: a date or datetime
        :param tzinfo: a boolean to indicate, whether datetime should be
                        returned with timezone
        :return: a datetime
        '''
        user_obj = Pool().get('res.user')
        tzlocal = dateutil.tz.tzlocal()

        user2 = user_obj.browse(user)
        tzuser = tzlocal
        if user2.timezone:
            tzuser = dateutil.tz.gettz(user2.timezone)
        if not isinstance(date, datetime.datetime):
            date = datetime.datetime.combine(date, datetime.time())\
                    .replace(tzinfo=tzlocal)
        else:
            date = date.replace(tzinfo=tzlocal)
        date = date.astimezone(tzuser)
        if not tzinfo:
            date = date.replace(tzinfo=None)
        return date

    def user2localtime(self, user, date, tzinfo=False):
        '''
        Return a datetime converted from user to server timezone.

        :param user: a user id
        :param date: a date or datetime
        :param tzinfo: a boolean to indicate, whether datetime should be
                        returned with timezone
        :return: a datetime
        '''
        user_obj = Pool().get('res.user')
        tzlocal = dateutil.tz.tzlocal()

        user2 = user_obj.browse(user)
        tzuser = tzlocal
        if user2.timezone:
            tzuser = dateutil.tz.gettz(user2.timezone)

        if not isinstance(date, datetime.datetime):
            date = datetime.datetime.combine(date, datetime.time())\
                    .replace(tzinfo=tzuser)
        else:
            date = date.replace(tzinfo=tzuser)
        date = date.astimezone(tzlocal)
        if not tzinfo:
            date = date.replace(tzinfo=None)
        return date

    def today(self):
        '''
        Current date in company time

        :return: a datetime.date
        '''
        today = super(Date, self).today()
        company = Transaction().context.get('company')
        if company:
            today = self.local2companytime(company, today).date()
        return today

Date()
