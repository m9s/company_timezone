# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import ModelView, ModelSQL, fields
import pytz

class Company(ModelSQL, ModelView):
    _name = 'company.company'

    timezone = fields.Selection('timezones', 'Timezone')

    def timezones(self):
        res = [(x, x) for x in pytz.common_timezones]
        return res

Company()
